# This file defines the software versions in use

class env::common::software_versions {
   $g5k_subnets = '1.4.2'
   $g5k_meta_packages = '0.7.22'
   $tgz_g5k = '1.0.12'
   $g5k_checks = '0.8.6'
   $sudo_g5k = '1.4'
}
